﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMover : MonoBehaviour {

    public float blockSpeed;
    private GameObject handObject;

    void Start ()
    {
        
	}
	
	void Update ()
    {
        //Move block forward
        transform.Translate(Vector3.back * blockSpeed * Time.deltaTime);

        //Translate block according to steering hand motion
       /*handObject = HandsTrackingController.Instance.trackedHand;

        if(handObject)
        {
            // var = Singleton, like    
            var handPosition = handObject.transform.position.x;
            var steeringVelocity = Mathf.Clamp(handPosition, -1, 1) * steeringSpeed;

            // Translate the blocks left or right, depending on the sign of the steering Velocity
            transform.Translate(Vector2.right * steeringVelocity * Time.deltaTime);
        } */
    }
}
