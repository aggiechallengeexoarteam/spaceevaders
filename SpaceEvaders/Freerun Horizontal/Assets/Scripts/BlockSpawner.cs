﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{
    public GameObject BlockPrefab;
    public GameObject Player;
    public float next_spawn_time;
    public float seconds_between_spawns; // Duration between space times
    private Bounds bounds;
    public float secs_until_despawn_cube;
    public float space_between_slots; // The space between the slots for a block to spawn in

    private int[] last_block_slots; // Store the last X slots a block was spawned in. Circular implementation
    private int current_index = 0; // Current index we're at in the last_block_slots array
    private int num_stored_slots = 4;

    private void Awake()
    {
        bounds = GetComponent<BoxCollider>().bounds;
        last_block_slots = new int[num_stored_slots];
    }

    // Update is called once per frame
    void Update ()
    {
        if (Time.time > next_spawn_time)
        {
            next_spawn_time = Time.time + seconds_between_spawns;

            // Start and end positions for where the blocks can spawn, relative to the Player
            float start_x =  -(bounds.size.x / 4); // The farthest left a block can spawn

            int slot = Random.Range(2, 8); // Choose one of the slots for the block to spawn in
            float block_width = BlockPrefab.GetComponent<Renderer>().bounds.size.x; // Get the width of the block

            float offset = Random.Range(-0.5f, 0.5f);

            int userSlot = roundPositionToSlot(Player.transform.position.x); // User's position?
            //userSlot = 0;

            float x = (slot + userSlot) * (block_width + space_between_slots) + start_x + offset; // Calculate the x value for the block to spawn in

            //Debug.Log("UserX: " + bounds.size.x + " UserSlot: " + userSlot + " blockX: " + x + " blockSlot " + slot);

            float y = transform.position.y;
            float z = transform.position.z;

            Vector3 spawn_position = new Vector3(x, y, z);
            GameObject newBlock = (GameObject)Instantiate(BlockPrefab, spawn_position, transform.rotation);

            Object.Destroy(newBlock, secs_until_despawn_cube);
        }
    }

    // Round the users position to a slot
    private int roundPositionToSlot(float x)
    {
        float rounded = x / (BlockPrefab.GetComponent<Renderer>().bounds.size.x + space_between_slots);

        return (int) System.Math.Floor(rounded);
    }

    // Get a random slot to spawn a block in, that isn't one of the last few slots
    private int getRandomSlot()
    {
        int rand = Random.Range(0, 10);
        while (inLastSlots(rand))
        {
            rand = Random.Range(0, 10);
        }

        return rand;
    }

    // Return if this slot was in one of the last few slots
    private bool inLastSlots(int slot)
    {
        for(int i = 0; i < num_stored_slots; i++)
        {
            if(slot == last_block_slots[i])
            {
                return true;
            }
        }

        return false;
    }

    private int getNextIndex()
    {
        return (current_index + 1) % num_stored_slots;
    }
}
