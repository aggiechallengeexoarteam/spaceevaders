﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthSystem : MonoBehaviour
{
    public int startingHealth;                             // The amount of health the player starts the game with.
    private int currentHealth;                                   // The current health the player has.
    public Slider healthSlider;                         // Reference to the UI's health bar.
    public RawImage damageVignette;
    private bool showDamaged;

    private void Awake()
    {
        currentHealth = startingHealth;
        healthSlider.value = startingHealth;
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Block"))
        {
            currentHealth--;
            showDamaged = true;
        }
    }
    void Update()
    {
        healthSlider.value = currentHealth;

        if (currentHealth == 0)
        {
            SceneManager.LoadScene("Game Over");
        }       

        if(showDamaged)
        {
            Color opaque = new Color(1, 1, 1, 1);
            damageVignette.color = Color.Lerp(damageVignette.color, opaque, 20 * Time.deltaTime);
            if(damageVignette.color.a > 0.9)
            {
                showDamaged = false;
            }
        }
        else
        {
            Color transparent = new Color(1, 1, 1, 0);
            damageVignette.color = Color.Lerp(damageVignette.color, transparent, 5 * Time.deltaTime);
        }
    }
}
