﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameStart : MonoBehaviour {

    public float countdownTime;
    private float timer;
    private TextMeshProUGUI countdownText;

	// Use this for initialization
	void Start () {
        timer = countdownTime;
        countdownText = GetComponentInParent<TextMeshProUGUI>();
        countdownText.text = "";
	}
	
	// Update is called once per frame
	void Update () {
        if(timer<=0)
        {
            SceneManager.LoadScene("level1");
        }

		if(HandsTrackingController.Instance.trackedHand)
        {
            timer -= Time.deltaTime;
            countdownText.text = "Game begins in: " + ((int)(timer+1)).ToString();
        }
        else
        {
            timer = countdownTime;
            countdownText.text = "";
        }
	}
}
