﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOver : MonoBehaviour {

    private TextMeshProUGUI gameOverText;

	// Use this for initialization
	void Start () {
        gameOverText = (TextMeshProUGUI)GetComponentInParent(typeof(TextMeshProUGUI));
        gameOverText.text = "Game Over" +
                            "\nYour Score: " + PlayerPrefs.GetInt("Score").ToString() +
                            "\nHigh Score: " + PlayerPrefs.GetInt("High Score").ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
