﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{

    public GameObject BlockPrefab;
    public float next_spawn_time;
    public float seconds_between_spawns;
    private Bounds bounds;
    public float secs_until_despawn_cube;

    private void Awake()
    {
        bounds = GetComponent<BoxCollider>().bounds;

    }

    // Update is called once per frame
    void Update ()
    {
        if (Time.time > next_spawn_time)
        {
            next_spawn_time = Time.time + seconds_between_spawns;

            float x = Random.Range(transform.position.x - bounds.extents.x, transform.position.x + bounds.extents.x);
            float y = Random.Range(transform.position.y - bounds.extents.y, transform.position.y + bounds.extents.y);
            float z = transform.position.z;
            Vector3 spawn_position = new Vector3(x, y, z);
            GameObject newBlock = (GameObject)Instantiate(BlockPrefab, spawn_position, transform.rotation);

            Object.Destroy(newBlock, secs_until_despawn_cube);
        }
    }
}
