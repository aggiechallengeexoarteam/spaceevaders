﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockMover : MonoBehaviour {

    public float blockSpeed;
    public float steeringSpeed;
    private GameObject handObject;

    void Start ()
    {
        
	}
	
	void Update ()
    {
        //Move block forward
        transform.Translate(Vector3.back * blockSpeed * Time.deltaTime);


        //Translate block according to steering hand motion
        handObject = HandsTrackingController.Instance.trackedHand;

        if(handObject)
        {
            var handPosition = handObject.transform.position.y;
            var steeringVelocity = Mathf.Clamp(handPosition, -1, 1) * steeringSpeed;
            transform.Translate(Vector2.up * steeringVelocity * Time.deltaTime);
        }
        
    }
}
