﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreCounter : MonoBehaviour
{
    private TextMeshProUGUI Score_text;
    private float score;
    public float scorePerSecond;

    private void Awake()
    {
        Score_text = GetComponent<TextMeshProUGUI>();
    }

    private void Start()
    {
        score = 0;
        Score_text.text = "";
    }

    void Update()
    {
        score += scorePerSecond * Time.deltaTime;
        //Debug.Log(Mathf.Round(count));
        Score_text.text = "Score: " + ((int)score).ToString();
    }

    private void OnDestroy()
    {
        PlayerPrefs.SetInt("Score", (int)score);
        if(score>PlayerPrefs.GetInt("High Score"))
        {
            PlayerPrefs.SetInt("High Score", (int)score);
        }

        PlayerPrefs.Save();
    }
}
